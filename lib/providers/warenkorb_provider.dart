import 'package:bremer_philharmoniker/models/bestellung.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class WarenkorbItem {
  final String konzertId;
  final String konzertTitle;
  final int kartenAnzahl;
  final double preis;

  WarenkorbItem({
    required this.konzertId,
    required this.konzertTitle,
    required this.kartenAnzahl,
    required this.preis,
  });
}

class WarenkorbProvider extends ChangeNotifier {
  Map<String, WarenkorbItem> _items = {};

  Map<String, WarenkorbItem> get items {
    return {..._items};
  }

  double get gesamtPreis {
    var gesamt = 0.0;
    _items.forEach((key, warenkorbItem) {
      gesamt += warenkorbItem.preis;
    });
    return gesamt;
  }

  void hinzufuegen(
      String id, String title, int kartenAnzahl, double einzelKartePreis) {
    if (_items.containsKey(id)) {
      _items.update(
          id,
          (existenteWarenkorbItem) => WarenkorbItem(
                konzertId: existenteWarenkorbItem.konzertId,
                konzertTitle: existenteWarenkorbItem.konzertTitle,
                kartenAnzahl:
                    existenteWarenkorbItem.kartenAnzahl + kartenAnzahl,
                preis: existenteWarenkorbItem.preis +
                    (kartenAnzahl * einzelKartePreis),
              ));
    } else {
      _items.putIfAbsent(
          id,
          () => WarenkorbItem(
                konzertId: id,
                konzertTitle: title,
                kartenAnzahl: kartenAnzahl,
                preis: kartenAnzahl * einzelKartePreis,
              ));
    }
    notifyListeners();
  }

  void entfernen(String id, int kartenAnzahl, double einzelKartePreis) {
    if (kartenAnzahl != 0 && _items[id]!.kartenAnzahl - kartenAnzahl == 0) {
      _items.remove(id);
    } else {
      _items.update(
          id,
          (existenteWarenkorbItem) => WarenkorbItem(
                konzertId: existenteWarenkorbItem.konzertId,
                konzertTitle: existenteWarenkorbItem.konzertTitle,
                kartenAnzahl:
                    existenteWarenkorbItem.kartenAnzahl - kartenAnzahl,
                preis: existenteWarenkorbItem.preis -
                    (kartenAnzahl * einzelKartePreis),
              ));
    }
    notifyListeners();
  }

  void kartenAnzahlBearbeiten(
      String id, int neuKartenAnzahl, double einzelKartePreis) {
    _items.update(
        id,
        (warenkorbItem) => WarenkorbItem(
            konzertId: warenkorbItem.konzertId,
            konzertTitle: warenkorbItem.konzertTitle,
            kartenAnzahl: neuKartenAnzahl,
            preis: neuKartenAnzahl * einzelKartePreis));
    notifyListeners();
  }

  void clearWarenkorb() {
    _items = {};
    notifyListeners();
  }

  Map<String, Bestellung> _bestellungen = {};

  Map<String, Bestellung> get bestellungen {
    return {..._bestellungen};
  }

  void bestellungHinzufuegen(Bestellung newBestellung) {
    _bestellungen.putIfAbsent(newBestellung.bestellungsId, () => newBestellung);
    notifyListeners();
  }
}

final warenkorbProvider =
    ChangeNotifierProvider<WarenkorbProvider>((ref) => WarenkorbProvider());
