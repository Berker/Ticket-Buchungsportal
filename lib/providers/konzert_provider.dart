import 'dart:convert';

import 'package:bremer_philharmoniker/models/konzert_stueck.dart';
import 'package:bremer_philharmoniker/models/konzertsaal.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;

import '../models/konzert.dart';

class KonzertProvider {
//  Konzert _selectedKonzert = konzerte[0];

//  Konzert get selectedKonzert => _selectedKonzert;

//  set selectedKonzert(Konzert selectedKonzert) {
//    _selectedKonzert = selectedKonzert;
//  }
  // DUMMY DATA
  List<Konzert> _konzerte = [];

  List<Konzert> get konzerte {
    return [..._konzerte];
  }

  Future<void> fetchAndSetKonzerte() async {
    var url = Uri.https("hier kommt die firebase addresse", "/konzerte.json");
    http.get(url);
    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      print(extractedData);
      if (extractedData == null) {
        return;
      }

      final List<Konzert> loadedKonzerte = [];
      extractedData.forEach((konzertId, konzertData) {
        loadedKonzerte.add(Konzert.fromMap(konzertData));
      });
      _konzerte = loadedKonzerte;
    } catch (error) {
      throw error;
    }
  }

  Konzert findById(String id) {
    return _konzerte.firstWhere((konzert) => konzert.id == id);
  }
}

// PROVIDERS
final konzertProvider = Provider<KonzertProvider>((ref) {
  return KonzertProvider();
});

/*final selectedKonzertProvider = StateProvider<String>((ref) {
  String ersteKonzert = ref.watch(konzertProvider).konzerte[0].id;
  return ersteKonzert;
});*/

final selectedKonzertProvider = StateProvider<Konzert>((ref) {
  Konzert ersteKonzert = ref.watch(konzertProvider).konzerte[0];
  return ersteKonzert;
});
