import 'package:bremer_philharmoniker/constants/app_assets.dart';
import 'package:bremer_philharmoniker/providers/warenkorb_provider.dart';
import 'package:bremer_philharmoniker/widgets/warenkorbFenster.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import 'package:badges/badges.dart';

class AppBarBrePhi extends StatelessWidget implements PreferredSizeWidget {
  const AppBarBrePhi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Center(
        child: Container(
          width: 250,
          child: Image.asset(AppAssets.evLogo),

          /*SvgPicture.asset(
            AppAssets.logo,
            fit: BoxFit.contain,
          ), */
        ),
      ),
      actions: [
        Consumer(builder: (context, ref, child) {
          return Padding(
            padding: const EdgeInsets.all(5.0),
            child: Badge(animationDuration: Duration(seconds: 3),
                badgeContent:
                    Text(ref.watch(warenkorbProvider).items.length.toString()),
                position: BadgePosition.topStart(),
                alignment: Alignment.center,
                elevation: 5,
                //value: ,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                      icon: Icon(ref.watch(warenkorbProvider).items.isEmpty ? Icons.shopping_cart_outlined : Icons.shopping_cart, size: 30,),
                      onPressed: () {
                        Navigator.of(context).pushNamed(WarenkorbFenster.routeName);
                      }),
                )),
          );
        }),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
