import 'package:bremer_philharmoniker/constants/text_styles.dart';
import 'package:bremer_philharmoniker/models/konzertsaal.dart';
import 'package:bremer_philharmoniker/providers/konzert_provider.dart';
import 'package:bremer_philharmoniker/widgets/konzert_item.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

//typedef void KonzertIdCallback(String konzertId);

class KonzertListItem extends ConsumerWidget {
  const KonzertListItem(
      {Key? key,
      required this.konzertId,
      required this.title,
      required this.konzertsaal,
      required this.konzertTermin})
      : super(key: key);

  final String konzertId;
  final String title;
  final Konzertsaal konzertsaal;
  final String konzertTermin;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    DateTime termin = DateTime.parse(konzertTermin);
    double _screenWidth = MediaQuery.of(context).size.width;

    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: _screenWidth < 620
              ? BorderRadius.circular(20)
              : BorderRadius.horizontal(right: Radius.circular(40))),
      elevation: 8,
      margin: _screenWidth < 620
          ? EdgeInsets.only(left: 10, right: 10, top: 10)
          : EdgeInsets.only(left: 7, right: 25, top: 10),
      child: ListTile(
          shape: RoundedRectangleBorder(
              borderRadius: _screenWidth < 620
                  ? BorderRadius.circular(20)
                  : BorderRadius.only(
                      topRight: Radius.circular(30),
                      bottomRight: Radius.circular(30))),
          hoverColor: Colors.lightGreen,
          selectedTileColor: Colors.lightGreen,
          title: Text(
            title,
            style: TextStyles.heading,
            textAlign: TextAlign.center,
          ),
          subtitle: Text(
            "${DateFormat("EEEE").format(termin)} | ${DateFormat("d MMM").format(termin)} | ${konzertsaal.name}",
            style: TextStyles.shortInfo,
            textAlign: TextAlign.center,
          ),
          onTap: () {
            ref.read(selectedKonzertProvider).state =
                ref.read(konzertProvider).findById(konzertId);
            if (MediaQuery.of(context).size.width < 620) {
              Navigator.pushNamed(context, KonzertItem.routeName);
            }
          }),
    );
  }
}
