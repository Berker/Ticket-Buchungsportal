import 'package:bremer_philharmoniker/constants/text_styles.dart';
import 'package:bremer_philharmoniker/models/konzert.dart';
import 'package:bremer_philharmoniker/providers/konzert_provider.dart';
import 'package:bremer_philharmoniker/widgets/app_bar_brephi.dart';
import 'package:bremer_philharmoniker/widgets/drawer_brephi.dart';
import 'package:bremer_philharmoniker/widgets/karten_bestellen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

class KonzertItem extends ConsumerStatefulWidget {
  static const routeName = "/konzert-item";

  @override
  _KonzertItemState createState() => _KonzertItemState();
}

class _KonzertItemState extends ConsumerState<KonzertItem>
    with SingleTickerProviderStateMixin {
  late final AnimationController _controller;
  late final Animation<double> _animation;
  late Konzert _konzert;

  Widget _konzertItemWidget() {
    return FadeTransition(
      opacity: _animation,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
        elevation: 8.0,
        margin: EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "${DateFormat("EEEE").format(DateTime.parse(_konzert.konzerttermin))} | "
                  "${DateFormat("d. MMMM").format(DateTime.parse(_konzert.konzerttermin))} | "
                  "${_konzert.konzertVenue.name} | "
                  "${DateFormat("HH:mm").format(DateTime.parse(_konzert.konzerttermin))}",
                  style: TextStyles.content,
                ),
              ),
              Divider(
                indent: 250,
                endIndent: 250,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30),
                child: FractionallySizedBox(
                  alignment: Alignment.center,
                  widthFactor: 0.5,
                  child: Container(
                      decoration: BoxDecoration(),
                      alignment: Alignment.center,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image.network(_konzert.bild),
                      )),
                ),
              ),
              FractionallySizedBox(
                widthFactor: 0.7,
                child: Container(
                  decoration: BoxDecoration(),
                  alignment: Alignment.center,
                  child: Text(
                    _konzert.langeInfo,
                    style: TextStyles.content,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Text(
                  "Künstler",
                  style: TextStyle(fontSize: 27, fontWeight: FontWeight.bold),
                ),
              ),
              for (String soloist in _konzert.solisten)
                Text(
                  soloist,
                  style: TextStyles.content,
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildMobileUI() {
    return Scaffold(
      appBar: AppBarBrePhi(),
      drawer: DrawerBrePhi(),
      body: Container(height: MediaQuery.of(context).size.height,child: _konzertItemWidget()),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_shopping_cart_outlined),
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return _bestellungsDialog();
              });
        },
      ),
    );
  }

  Widget _bestellungsDialog() {
    return AlertDialog(
      backgroundColor: Colors.lightGreen,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      content: KartenBestellen(),
    );
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
    );
    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_controller);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _initKonzert();
  }

  void _initKonzert() {
    Konzert konzert = ref.watch(selectedKonzertProvider).state;
    setState(() {
      _konzert = konzert;
    });
    _controller.forward();
  }

  void _startAnimation() {
    Konzert konzert = ref.watch(selectedKonzertProvider).state;
    _controller.reverse();
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        _konzert = konzert;
      });
      _controller.forward();
    });
  }

  @override
  Widget build(BuildContext context) {
    ref.listen(selectedKonzertProvider, (value) {
      _startAnimation();
    });
    return MediaQuery.of(context).size.width < 620
        ? _buildMobileUI()
        : _konzertItemWidget();
  }
}
