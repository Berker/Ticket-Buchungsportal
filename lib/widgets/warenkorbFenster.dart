import 'package:bremer_philharmoniker/constants/text_styles.dart';
import 'package:bremer_philharmoniker/main.dart';
import 'package:bremer_philharmoniker/providers/konzert_provider.dart';
import 'package:bremer_philharmoniker/providers/warenkorb_provider.dart';
import 'package:bremer_philharmoniker/widgets/app_bar_brephi.dart';
import 'package:bremer_philharmoniker/widgets/bestellForm.dart';
import 'package:bremer_philharmoniker/widgets/drawer_brephi.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class WarenkorbFenster extends StatefulWidget {
  static const routeName = "/warenkorb-fenster";

  @override
  _WarenkorbFensterState createState() => _WarenkorbFensterState();
}

class _WarenkorbFensterState extends State<WarenkorbFenster> {
  bool _isEditingText = false;

  late TextEditingController _editingController;

  Widget _editkartenAnzahl(String konzertId, double einzelPreis,
      String kartenAnzahl, WidgetRef ref) {
    _editingController = TextEditingController(text: kartenAnzahl);
    if (_isEditingText) {
      return TextField(
        keyboardType: TextInputType.number,
        onSubmitted: (newValue) {
          kartenAnzahl = newValue;
          _isEditingText = false;
          ref.watch(warenkorbProvider).kartenAnzahlBearbeiten(
              konzertId, int.parse(kartenAnzahl), einzelPreis);
        },
        autofocus: true,
        controller: _editingController,
      );
    }
    return InkWell(
        onTap: () {
          setState(() {
            _isEditingText = true;
          });
        },
        child: Text(kartenAnzahl));
  }

  Widget _buildMobileUI() {
    return Consumer(
      builder: (context, ref, child) {
        final _konzert = ref.watch(konzertProvider);
        final _warenkorb = ref.watch(warenkorbProvider);
        return SingleChildScrollView(
          child: Column(crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40),
                ),
                elevation: 8.0,
                margin: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Warenkorb Übersicht",
                        style: TextStyles.heading
                            .copyWith(fontWeight: FontWeight.bold),
                      ),
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Column(
                        children: [
                          DataTable(
                            showBottomBorder: true,
                            columns: [
                              DataColumn(
                                  label: Text(
                                "Name",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 22),
                              )),
                              DataColumn(
                                  label: Text("Anzahl",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22))),
                              DataColumn(
                                  label: Text("Gesamt",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 22))),
                            ],
                            rows: _warenkorb.items.values
                                .map((item) => DataRow(cells: [
                                      DataCell(Text(item.konzertTitle)),
                                      DataCell(
                                        _editkartenAnzahl(
                                            item.konzertId,
                                            _konzert
                                                .findById(item.konzertId)
                                                .einzelKartePreis,
                                            item.kartenAnzahl.toString(),
                                            ref),
                                        showEditIcon: true,
                                      ),
                                      DataCell(Text(
                                          "${item.kartenAnzahl * _konzert.findById(item.konzertId).einzelKartePreis} €"))
                                    ]))
                                .toList(),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Gesamt: ${_warenkorb.gesamtPreis} €",
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ]),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: ElevatedButton.icon(
                                    icon: Icon(
                                        Icons.remove_shopping_cart),
                                    onPressed: () {
                                      _warenkorb.clearWarenkorb();
                                    },
                                    label: Text("Warenkorb leeren"),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: ElevatedButton.icon(
                                    icon: Icon(Icons.add),
                                    onPressed: () {
                                      Navigator.pushNamed(context,
                                          MyHomePage.routeName);
                                    },
                                    label: Text("Mehr karten"),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              BestellForm(),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarBrePhi(),
      drawer: DrawerBrePhi(),
      body: Consumer(
        builder: (context, ref, child) {
          final _warenkorb = ref.watch(warenkorbProvider);
          final _konzert = ref.watch(konzertProvider);
          return _warenkorb.items.isEmpty
              ? Center(
                  child: Text(
                    "Warenkorb ist leer!",
                    style: TextStyles.heading,
                  ),
                )
              : MediaQuery.of(context).size.width < 620
                  ? _buildMobileUI()
                  : Row(
                      children: [
                        BestellForm(),
                        Expanded(
                          child: FractionallySizedBox(
                            widthFactor: 1.0,
                            heightFactor: 1.0,
                            child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40)),
                              elevation: 8.0,
                              margin: EdgeInsets.all(10),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        "Warenkorb Übersicht",
                                        style: TextStyles.heading.copyWith(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    DataTable(
                                      showBottomBorder: true,
                                      columns: [
                                        DataColumn(
                                            label: Text(
                                          "Name",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 22),
                                        )),
                                        DataColumn(
                                            label: Text("Anzahl",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 22))),
                                        DataColumn(
                                            label: Text("Einzelpreis",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 22))),
                                        DataColumn(
                                            label: Text("Gesamt",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 22))),
                                      ],
                                      rows: _warenkorb.items.values
                                          .map((item) => DataRow(cells: [
                                                DataCell(
                                                    Text(item.konzertTitle)),
                                                DataCell(
                                                  _editkartenAnzahl(
                                                      item.konzertId,
                                                      _konzert
                                                          .findById(
                                                              item.konzertId)
                                                          .einzelKartePreis,
                                                      item.kartenAnzahl
                                                          .toString(),
                                                      ref),
                                                  showEditIcon: true,
                                                ),
                                                DataCell(Text(
                                                    "${_konzert.findById(item.konzertId).einzelKartePreis} €")),
                                                DataCell(Text(
                                                    "${item.kartenAnzahl * _konzert.findById(item.konzertId).einzelKartePreis} €"))
                                              ]))
                                          .toList(),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Gesamt: ${_warenkorb.gesamtPreis} €",
                                            style: TextStyle(
                                                fontSize: 22,
                                                fontWeight: FontWeight.bold),
                                          )
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: ElevatedButton.icon(
                                            icon: Icon(
                                                Icons.remove_shopping_cart),
                                            onPressed: () {
                                              _warenkorb.clearWarenkorb();
                                            },
                                            label: Text("Warenkorb leeren"),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: ElevatedButton.icon(
                                            icon: Icon(Icons.add),
                                            onPressed: () {
                                              Navigator.pushNamed(context,
                                                  MyHomePage.routeName);
                                            },
                                            label: Text("Mehr karten"),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
        },
      ),
    );
  }
}
