class KonzertStueck {
  String title;
  String komponist;
  String? extraInfo;

  KonzertStueck(
      {required this.title,
      required this.komponist,
      required this.extraInfo});

  factory KonzertStueck.fromMap(Map<String, dynamic> parsedJson) {
    final title = parsedJson["title"] as String;
    final komponist = parsedJson["komponist"] as String;
    final extraInfo = parsedJson["extraInfo"] as String?;

    return KonzertStueck(
        title: title,
        komponist: komponist,
        extraInfo: extraInfo);
  }
}
