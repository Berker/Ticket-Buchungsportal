import 'package:bremer_philharmoniker/models/konzert_stueck.dart';
import 'package:bremer_philharmoniker/models/konzertsaal.dart';

class Konzert {
  String id;
  String title;
  String kurzeInfo;
  String langeInfo;
  bool abgesagt;
  bool ausverkauft;
  List<dynamic> solisten;
  String dirigent;
  String konzerttermin;
  Konzertsaal konzertVenue;
  List<KonzertStueck> konzertProgram;
  String bild;
  int verkaufteKartenZahl;
  double einzelKartePreis;

  Konzert({
    required this.id,
    required this.title,
    required this.kurzeInfo,
    required this.langeInfo,
    this.abgesagt = false,
    this.ausverkauft = false,
    required this.solisten,
    required this.dirigent,
    required this.konzerttermin,
    required this.konzertVenue,
    required this.konzertProgram,
    required this.bild,
    this.verkaufteKartenZahl = 0,
    this.einzelKartePreis = 20,
  });

  factory Konzert.fromMap(Map<String, dynamic> parsedJson) {
    final id = parsedJson["id"] as String;
    //print(id);
    final title = parsedJson["title"] as String;
    //print(title);
    final kurzeInfo = parsedJson["kurzeInfo"] as String;
    //print(kurzeInfo);
    final langeInfo = parsedJson["langeInfo"] as String;
    //print(langeInfo);
    final bild = parsedJson["bild"] as String;
    //print(bild);
    final dirigent = parsedJson["dirigent"] as String;
    //print(dirigent);
    final konzerttermin = parsedJson["konzerttermin"] as String;
    //print(konzerttermin);
    final abgesagt = parsedJson["abgesagt"] as String == "true" ? true : false;
    //print(abgesagt);
    final ausverkauft =
        parsedJson["ausverkauft"] as String == "true" ? true : false;
    //print(ausverkauft);
    final einzelKartePreis = parsedJson["einzelKartePreis"] as double;
    //print(einzelKartePreis);
    final verkaufteKartenZahl = parsedJson["verkaufteKartenZahl"] as int;
    //print(verkaufteKartenZahl);

    final solisten =
        (parsedJson["solisten"] as List<dynamic>).asMap().values.toList();

    //print(solisten);
    final konzertProgram = (parsedJson["konzertProgram"] as List<dynamic>)
        .map((e) => KonzertStueck.fromMap(e))
        .toList();
    konzertProgram.forEach((element) {print(element.title);});
    final konzertVenue = Konzertsaal.fromJson(
        (parsedJson["konzertVenue"] as Map<String, dynamic>));
    //print(konzertVenue.name);

    return Konzert(
      id: id,
      title: title,
      kurzeInfo: kurzeInfo,
      langeInfo: langeInfo,
      bild: bild,
      dirigent: dirigent,
      konzerttermin: konzerttermin,
      abgesagt: abgesagt,
      ausverkauft: ausverkauft,
      einzelKartePreis: einzelKartePreis,
      verkaufteKartenZahl: verkaufteKartenZahl,
      solisten: solisten,
      konzertProgram: konzertProgram,
      konzertVenue: konzertVenue,
    );
  }
}
