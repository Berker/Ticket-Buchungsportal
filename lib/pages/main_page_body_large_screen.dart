import 'package:bremer_philharmoniker/constants/text_styles.dart';
import 'package:bremer_philharmoniker/models/konzert.dart';
import 'package:bremer_philharmoniker/providers/konzert_provider.dart';
import 'package:bremer_philharmoniker/widgets/karten_bestellen.dart';
import 'package:bremer_philharmoniker/widgets/konzert_item.dart';
import 'package:bremer_philharmoniker/widgets/konzert_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MainPageBodyLargeScreen extends ConsumerStatefulWidget {
  @override
  _MainPageBodyLargeScreenState createState() =>
      _MainPageBodyLargeScreenState();
}

class _MainPageBodyLargeScreenState
    extends ConsumerState<MainPageBodyLargeScreen> {
  var _isInit = false;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!_isInit) {
      setState(() {
        _isLoading = true;
      });

      ref.watch(konzertProvider).
          fetchAndSetKonzerte()
          .then((value) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = true;
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading ? Center(child: CircularProgressIndicator(),) : ref.watch(konzertProvider).konzerte.length == 0
        ? Center(
            child: Text(
              "Die Konzerte für nächste Saison wird bald aktualisiert!",
              style: TextStyles.heading,
            ),
          )
        : Row(
            children: [
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: FractionallySizedBox(
                  alignment: Alignment.centerLeft,
                  heightFactor: 1.0,
                  widthFactor: 0.5,
                  child: Container(
                    margin: EdgeInsets.only(top: 3, bottom: 3),
                    decoration: BoxDecoration(
                      color: Colors.lightGreen,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(40),
                        bottomRight: Radius.circular(40),
                      ),
                    ),
                    child: KonzertList(),
                  ),
                ),
              )),
              Expanded(
                child: FractionallySizedBox(
                  alignment: Alignment.center,
                  heightFactor: 1.0,
                  widthFactor: 2.0,
                  child: KonzertItem(),
                ),
              ),
              Expanded(
                  child: FractionallySizedBox(
                alignment: Alignment.centerRight,
                heightFactor: 1.0,
                widthFactor: 0.5,
                child: Container(
                    margin: EdgeInsets.only(top: 8, bottom: 8),
                    decoration: BoxDecoration(
                        color: Colors.lightGreen,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            bottomLeft: Radius.circular(40))),
                    child: KartenBestellen()),
              )),
            ],
          );
  }
}
