import 'package:bremer_philharmoniker/constants/bremer_philharmoniker_info.dart';
import 'package:bremer_philharmoniker/constants/text_styles.dart';
import 'package:bremer_philharmoniker/widgets/app_bar_brephi.dart';
import 'package:bremer_philharmoniker/widgets/drawer_brephi.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class AboutPage extends StatelessWidget {
  static const routeName = "/about-page";

  const AboutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    double _fontSizeHeader = MediaQuery.of(context).size.width < 620 ? 20 : 30;
    double _fontSizeContent = MediaQuery.of(context).size.width < 620 ? 20 : 25;

    return Scaffold(
      appBar: AppBarBrePhi(),
      drawer: DrawerBrePhi(),
      body: Center(
          child: Card(
            elevation: 15,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Service",
                    style: TextStyles.heading
                        .copyWith(fontWeight: FontWeight.bold, fontSize: _fontSizeHeader),
                  ),
                  Container(width: 12, child: Divider()),
                  Text(
                    "Bei Fragen hilft Ihnen unser Besucherservice gern weiter.",
                    textAlign: TextAlign.center,
                    style: TextStyles.content.copyWith(fontSize: _fontSizeContent),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(
                      BremerPhilharmonikerInfo.name,
                      style: TextStyles.content.copyWith(
                          fontWeight: FontWeight.bold, fontSize: _fontSizeContent),
                    ),
                  ),
                  Text(
                    "Besucherservice \n"
                    " Plantage 13 | 28215 Bremen"
                    " \n Ihre Ansprechpartnerin: \n"
                    " Silke Voss \n"
                    " \u260E ${BremerPhilharmonikerInfo.telefon} \n"
                    " \u2327 ${BremerPhilharmonikerInfo.email}",
                    textAlign: TextAlign.center,
                    style: TextStyles.content.copyWith(fontSize: _fontSizeContent),
                  ),
                ],
              ),
            ),
          ),
        ),
    );
  }
}
