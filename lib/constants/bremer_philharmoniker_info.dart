class BremerPhilharmonikerInfo {
  static const String name = "Bremer Philharmoniker GmbH";
  static const String adresse = "Plantage 13, 28215 Bremen";
  static const String telefon = "+49 (0) 421 - 62 67 312";
  static const String email = "info@bremerphilharmoniker.de";
}