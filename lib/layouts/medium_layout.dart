import 'package:bremer_philharmoniker/constants/app_assets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/svg.dart';

class MediumLayout extends StatelessWidget {
  const MediumLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Container(
              width: 250,
              child: SvgPicture.asset(
                AppAssets.logo,
                fit: BoxFit.contain,
              ),
            ),
          ),
        ),
        drawer: Drawer(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("this is it"),
            ],
          ),
        ),
        body: Center(
          child: Text("Bremer Philharmoniker medium"),
        ),
      ),
    );
  }
}