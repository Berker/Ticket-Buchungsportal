import 'package:bremer_philharmoniker/constants/app_assets.dart';
import 'package:bremer_philharmoniker/constants/text_styles.dart';
import 'package:bremer_philharmoniker/providers/konzert_provider.dart';
import 'package:bremer_philharmoniker/widgets/app_bar_brephi.dart';
import 'package:bremer_philharmoniker/widgets/drawer_brephi.dart';
import 'package:bremer_philharmoniker/widgets/konzert_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

class SmallLayout extends StatelessWidget {
  const SmallLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBarBrePhi(),
        drawer: DrawerBrePhi(),
        body: Consumer(
          builder: (BuildContext context, WidgetRef ref, child) {
            return ref.watch(konzertProvider).konzerte.length == 0
                ? Center(
                    child: Text(
                      "Die Konzerte für nächste Saison wird bald aktualisiert!",
                      style: TextStyles.heading,
                    ),
                  )
                : KonzertList();
          },
        ),
      ),
    );
  }
}
